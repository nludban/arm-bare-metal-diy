#######################################################################
arm-bare-metal-diy/README.rst
#######################################################################

This was a presentation given at the columbus arduino raspberry pi
enthusiast (CARPE) meetup 2018-07-10.  Clone the repo and open
presentation.hml in a browser to view (make the font bigger and resize
the window so the blue headers make minimum height "slides").

***********************************************************************
Abstract
***********************************************************************

The ARM Cortex microcontroller core is widely used on maker boards,
many of which are supported by Arduino IDE or similar.
These GUIs make it easy to get started, but at the same time can stand
in the way of deeper understanding, features, and performance.
This presentation goes behind the scenes and gives an overview of
common tools used for debugging, how an embedded application is
assembled and starts up, adds some C++ support, and ends with an
example of handling interrupts to switch between threads.

Target audience is intermediate-advanced.

TODO: Update everything for ATSAMD51 (eg, adafruit metro m4 express)
which is less powerful than the K64F but also much simpler.
Clean up the mtk library and release it.
